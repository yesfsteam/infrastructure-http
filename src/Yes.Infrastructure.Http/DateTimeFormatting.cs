﻿﻿ namespace Yes.Infrastructure.Http
{
    public enum DateTimeFormatting
    {
        /// <summary>
        /// Сериализация DateTime в стандартный формат
        /// </summary>
        Default,
        /// <summary>
        /// Сериализация DateTime с явным указанием часового пояса +03:00
        /// </summary>
        MoscowTime
    }
}