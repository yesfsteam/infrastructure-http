﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Yes.Infrastructure.Http.Extensions;

namespace Yes.Infrastructure.Http
{
    public class RestClientBase
    {
        protected readonly HttpClient HttpClient;
        private readonly JsonSerializerSettings jsonSerializerSettings;
        private readonly Uri baseUri;
		
        /// <summary>
        /// Для случаев, когда базовый uri неизвестен
        /// </summary>
        public RestClientBase(HttpClient httpClient, JsonSerializerSettings jsonSerializerSettings = null)
        {
	        HttpClient = httpClient;
	        this.jsonSerializerSettings = jsonSerializerSettings;
        }
        
        /// <summary>
        /// Для случаев, когда у нас всегда фиксированный адрес (baseUrl)
        /// </summary>
        public RestClientBase(HttpClient httpClient, string baseUri, JsonSerializerSettings jsonSerializerSettings = null)
        {
            this.baseUri = new Uri(baseUri);
            HttpClient = httpClient;
            this.jsonSerializerSettings = jsonSerializerSettings;
        }

		#region Get
		
		public Response GetSync(string uri)
		{
			var requestUri = getRequestUri(uri);
			var response = HttpClient.GetAsync(requestUri).GetSynchronousResult();
			return createResponseSync(response);
		}
		
		public Response GetSync(string uri, object queryStringValues)
		{
			var queryString = getQueryString(uri, queryStringValues);
			return GetSync($"{uri}{queryString}");
		}

		public Response<T> GetSync<T>(string uri)
        {
            var requestUri = getRequestUri(uri);
            var response = HttpClient.GetAsync(requestUri).GetSynchronousResult();
            return createResponseSync<T>(response);
        }

        public Response<T> GetSync<T>(string uri, object queryStringValues)
        {
	        var queryString = getQueryString(uri, queryStringValues);
            return GetSync<T>($"{uri}{queryString}");
        }

        public Response<TSuccessModel, TBadRequestErrorModel> GetSync<TSuccessModel, TBadRequestErrorModel>(string uri)
        {
            var requestUri = getRequestUri(uri);
            var response = HttpClient.GetAsync(requestUri).GetSynchronousResult();
            return createResponseSync<TSuccessModel, TBadRequestErrorModel>(response);
        }

        public Response<TSuccessModel, TBadRequestErrorModel> GetSync<TSuccessModel, TBadRequestErrorModel>(string uri, object queryStringValues)
        {
	        var queryString = getQueryString(uri, queryStringValues);
            return GetSync<TSuccessModel, TBadRequestErrorModel>($"{uri}{queryString}");
        }
		
        public async Task<Response> Get(string uri)
        {
	        var requestUri = getRequestUri(uri);
	        var response = await HttpClient.GetAsync(requestUri);
	        return await createResponse(response);
        }
		
        public async Task<Response> Get(string uri, object queryStringValues)
        {
	        var queryString = getQueryString(uri, queryStringValues);
	        return await Get($"{uri}{queryString}");
        }
		
        public async Task<Response<T>> Get<T>(string uri)
        {
            var requestUri = getRequestUri(uri);
            var response = await HttpClient.GetAsync(requestUri);
            return await createResponse<T>(response);
        }

        public async Task<Response<T>> Get<T>(string uri, object queryStringValues)
        {
	        var queryString = getQueryString(uri, queryStringValues);
            return await Get<T>($"{uri}{queryString}");
        }

        public async Task<Response<TSuccessModel, TBadRequestErrorModel>> Get<TSuccessModel, TBadRequestErrorModel>(string uri)
        {
            var requestUri = getRequestUri(uri);
            var response = await HttpClient.GetAsync(requestUri);
            return await createResponse<TSuccessModel, TBadRequestErrorModel>(response);
        }

        public async Task<Response<TSuccessModel, TBadRequestErrorModel>> Get<TSuccessModel, TBadRequestErrorModel>(string uri, object queryStringValues)
        {
	        var queryString = getQueryString(uri, queryStringValues);
	        return await Get<TSuccessModel, TBadRequestErrorModel>($"{uri}{queryString}");
        }
		
        public HttpResponseMessage SimpleGetSync(string uri)
        {
	        var requestUri = getRequestUri(uri);
	        var response = HttpClient.GetAsync(requestUri).GetSynchronousResult();
	        return response;
        }

        public HttpResponseMessage SimpleGetSync(string uri, object queryStringValues)
        {
	        var queryString = getQueryString(uri, queryStringValues);
	        return SimpleGetSync($"{uri}{queryString}");
        }

        public async Task<HttpResponseMessage> SimpleGet(string uri)
        {
	        var requestUri = getRequestUri(uri);
	        var response = await HttpClient.GetAsync(requestUri);
	        return response;
        }
		
        public async Task<HttpResponseMessage> SimpleGet(string uri, object queryStringValues)
        {
	        var queryString = getQueryString(uri, queryStringValues);
	        return await SimpleGet($"{uri}{queryString}");
        }

		#endregion

		#region Post
		
		public Response PostSync(string uri)
		{
			var requestUri = getRequestUri(uri);
			var response = HttpClient.PostAsync(requestUri, null).GetSynchronousResult();
			return createResponseSync(response);
		}
		
		public Response PostSync(string uri, object obj)
		{
			var requestUri = getRequestUri(uri);
			var content = new JsonContent(obj, jsonSerializerSettings);
			var response = HttpClient.PostAsync(requestUri, content).GetSynchronousResult();
			return createResponseSync(response);
		}
		
		public Response<T> PostSync<T>(string uri)
		{
			var requestUri = getRequestUri(uri);
			var response = HttpClient.PostAsync(requestUri, null).GetSynchronousResult();
			return createResponseSync<T>(response);
		}

		public Response<TSuccessModel, TBadRequestErrorModel> PostSync<TSuccessModel, TBadRequestErrorModel>(string uri)
		{
			var requestUri = getRequestUri(uri);
			var response = HttpClient.PostAsync(requestUri, null).GetSynchronousResult();
			return createResponseSync<TSuccessModel, TBadRequestErrorModel>(response);
		}

		public Response<T> PostSync<T>(string uri, object obj)
		{
			var requestUri = getRequestUri(uri);
			var content = new JsonContent(obj, jsonSerializerSettings);
			var response = HttpClient.PostAsync(requestUri, content).GetSynchronousResult();
			return createResponseSync<T>(response);
		}

		public Response<TSuccessModel, TBadRequestErrorModel> PostSync<TSuccessModel, TBadRequestErrorModel>(string uri, object obj)
		{
			var requestUri = getRequestUri(uri);
			var content = new JsonContent(obj, jsonSerializerSettings);
			var response = HttpClient.PostAsync(requestUri, content).GetSynchronousResult();
			return createResponseSync<TSuccessModel, TBadRequestErrorModel>(response);
		}
		
		public async Task<Response> Post(string uri)
		{
			var requestUri = getRequestUri(uri);
			var response = await HttpClient.PostAsync(requestUri, null);
			return await createResponse(response);
		}
		
		public async Task<Response> Post(string uri, object obj)
		{
			var requestUri = getRequestUri(uri);
			var content = new JsonContent(obj, jsonSerializerSettings);
			var response = await HttpClient.PostAsync(requestUri, content);
			return await createResponse(response);
		}

		public async Task<Response<T>> Post<T>(string uri)
		{
			var requestUri = getRequestUri(uri);
			var response = await HttpClient.PostAsync(requestUri, null);
			return await createResponse<T>(response);
		}

		public async Task<Response<TSuccessModel, TBadRequestErrorModel>> Post<TSuccessModel, TBadRequestErrorModel>(string uri)
		{
			var requestUri = getRequestUri(uri);
			var response = await HttpClient.PostAsync(requestUri, null);
			return await createResponse<TSuccessModel, TBadRequestErrorModel>(response);
		}

		public async Task<Response<T>> Post<T>(string uri, object obj)
		{
			var requestUri = getRequestUri(uri);
			var content = new JsonContent(obj, jsonSerializerSettings);
			var response = await HttpClient.PostAsync(requestUri, content);
			return await createResponse<T>(response);
		}

		public async Task<Response<TSuccessModel, TBadRequestErrorModel>> Post<TSuccessModel, TBadRequestErrorModel>(string uri, object obj)
		{
			var requestUri = getRequestUri(uri);
			var content = new JsonContent(obj, jsonSerializerSettings);
			var response = await HttpClient.PostAsync(requestUri, content);
			return await createResponse<TSuccessModel, TBadRequestErrorModel>(response);
		}
		
		public HttpResponseMessage SimplePostSync(string uri)
		{
			var requestUri = getRequestUri(uri);
			var response = HttpClient.PostAsync(requestUri, null).GetSynchronousResult();
			return response;
		}

		public HttpResponseMessage SimplePostSync(string uri, object obj)
		{
			var requestUri = getRequestUri(uri);
			var content = new JsonContent(obj, jsonSerializerSettings);
			var response = HttpClient.PostAsync(requestUri, content).GetSynchronousResult();
			return response;
		}

		public async Task<HttpResponseMessage> SimplePost(string uri)
		{
			var requestUri = getRequestUri(uri);
			var response = await HttpClient.PostAsync(requestUri, null);
			return response;
		}
		
		public async Task<HttpResponseMessage> SimplePost(string uri, object obj)
		{
			var requestUri = getRequestUri(uri);
			var content = new JsonContent(obj, jsonSerializerSettings);
			var response = await HttpClient.PostAsync(requestUri, content);
			return response;
		}
		
		#endregion

		#region Put
		
		public Response PutSync(string uri)
		{
			var requestUri = getRequestUri(uri);
			var response = HttpClient.PutAsync(requestUri, null).GetSynchronousResult();
			return createResponseSync(response);
		}
		
		public Response PutSync(string uri, object obj)
		{
			var requestUri = getRequestUri(uri);
			var content = new JsonContent(obj, jsonSerializerSettings);
			var response = HttpClient.PutAsync(requestUri, content).GetSynchronousResult();
			return createResponseSync(response);
		}

		public Response<T> PutSync<T>(string uri)
		{
			var requestUri = getRequestUri(uri);
			var response = HttpClient.PutAsync(requestUri, null).GetSynchronousResult();
			return createResponseSync<T>(response);
		}

		public Response<TSuccessModel, TBadRequestErrorModel> PutSync<TSuccessModel, TBadRequestErrorModel>(string uri)
		{
			var requestUri = getRequestUri(uri);
			var response = HttpClient.PutAsync(requestUri, null).GetSynchronousResult();
			return createResponseSync<TSuccessModel, TBadRequestErrorModel>(response);
		}

		public Response<T> PutSync<T>(string uri, object obj)
		{
			var requestUri = getRequestUri(uri);
			var content = new JsonContent(obj, jsonSerializerSettings);
			var response = HttpClient.PutAsync(requestUri, content).GetSynchronousResult();
			return createResponseSync<T>(response);
		}

		public Response<TSuccessModel, TBadRequestErrorModel> PutSync<TSuccessModel, TBadRequestErrorModel>(string uri, object obj)
		{
			var requestUri = getRequestUri(uri);
			var content = new JsonContent(obj, jsonSerializerSettings);
			var response = HttpClient.PutAsync(requestUri, content).GetSynchronousResult();
			return createResponseSync<TSuccessModel, TBadRequestErrorModel>(response);
		}
		
		public async Task<Response> Put(string uri)
		{
			var requestUri = getRequestUri(uri);
			var response = await HttpClient.PutAsync(requestUri, null);
			return await createResponse(response);
		}
		
		public async Task<Response> Put(string uri, object obj)
		{
			var requestUri = getRequestUri(uri);
			var content = new JsonContent(obj, jsonSerializerSettings);
			var response = await HttpClient.PutAsync(requestUri, content);
			return await createResponse(response);
		}

		public async Task<Response<T>> Put<T>(string uri)
		{
			var requestUri = getRequestUri(uri);
			var response = await HttpClient.PutAsync(requestUri, null);
			return await createResponse<T>(response);
		}

		public async Task<Response<TSuccessModel, TBadRequestErrorModel>> Put<TSuccessModel, TBadRequestErrorModel>(string uri)
		{
			var requestUri = getRequestUri(uri);
			var response = await HttpClient.PutAsync(requestUri, null);
			return await createResponse<TSuccessModel, TBadRequestErrorModel>(response);
		}

		public async Task<Response<T>> Put<T>(string uri, object obj)
		{
			var requestUri = getRequestUri(uri);
			var content = new JsonContent(obj, jsonSerializerSettings);
			var response = await HttpClient.PutAsync(requestUri, content);
			return await createResponse<T>(response);
		}

		public async Task<Response<TSuccessModel, TBadRequestErrorModel>> Put<TSuccessModel, TBadRequestErrorModel>(string uri, object obj)
		{
			var requestUri = getRequestUri(uri);
			var content = new JsonContent(obj, jsonSerializerSettings);
			var response = await HttpClient.PutAsync(requestUri, content);
			return await createResponse<TSuccessModel, TBadRequestErrorModel>(response);
		}
		
		public HttpResponseMessage SimplePutSync(string uri)
		{
			var requestUri = getRequestUri(uri);
			var response = HttpClient.PutAsync(requestUri, null).GetSynchronousResult();
			return response;
		}

		public HttpResponseMessage SimplePutSync(string uri, object obj)
		{
			var requestUri = getRequestUri(uri);
			var content = new JsonContent(obj, jsonSerializerSettings);
			var response = HttpClient.PutAsync(requestUri, content).GetSynchronousResult();
			return response;
		}

		public async Task<HttpResponseMessage> SimplePut(string uri)
		{
			var requestUri = getRequestUri(uri);
			var response = await HttpClient.PutAsync(requestUri, null);
			return response;
		}
		
		public async Task<HttpResponseMessage> SimplePut(string uri, object obj)
		{
			var requestUri = getRequestUri(uri);
			var content = new JsonContent(obj, jsonSerializerSettings);
			var response = await HttpClient.PutAsync(requestUri, content);
			return response;
		}

		#endregion
		
		#region Delete
		
		public Response DeleteSync(string uri)
		{
			var requestUri = getRequestUri(uri);
			var response = HttpClient.DeleteAsync(requestUri).GetSynchronousResult();
			return createResponseSync(response);
		}
		
		public Response DeleteSync(string uri, object obj)
		{
			var requestUri = getRequestUri(uri);
			var content = new JsonContent(obj, jsonSerializerSettings);
			var request = new HttpRequestMessage { Content = content, Method = HttpMethod.Delete, RequestUri = requestUri };
			var response = HttpClient.SendAsync(request).GetSynchronousResult();
			return createResponseSync(response);
		}

		public Response<T> DeleteSync<T>(string uri)
		{
			var requestUri = getRequestUri(uri);
			var response = HttpClient.DeleteAsync(requestUri).GetSynchronousResult();
			return createResponseSync<T>(response);
		}

		public Response<TSuccessModel, TBadRequestErrorModel> DeleteSync<TSuccessModel, TBadRequestErrorModel>(string uri)
		{
			var requestUri = getRequestUri(uri);
			var response = HttpClient.DeleteAsync(requestUri).GetSynchronousResult();
			return createResponseSync<TSuccessModel, TBadRequestErrorModel>(response);
		}

		public Response<T> DeleteSync<T>(string uri, object obj)
		{
			var requestUri = getRequestUri(uri);
			var content = new JsonContent(obj, jsonSerializerSettings);
			var request = new HttpRequestMessage { Content = content, Method = HttpMethod.Delete, RequestUri = requestUri };
			var response = HttpClient.SendAsync(request).GetSynchronousResult();
			return createResponseSync<T>(response);
		}

		public Response<TSuccessModel, TBadRequestErrorModel> DeleteSync<TSuccessModel, TBadRequestErrorModel>(string uri, object obj)
		{
			var requestUri = getRequestUri(uri);
			var content = new JsonContent(obj, jsonSerializerSettings);
			var request = new HttpRequestMessage { Content = content, Method = HttpMethod.Delete, RequestUri = requestUri };
			var response = HttpClient.SendAsync(request).GetSynchronousResult();
			return createResponseSync<TSuccessModel, TBadRequestErrorModel>(response);
		}
		
		public async Task<Response> Delete(string uri)
		{
			var requestUri = getRequestUri(uri);
			var response = await HttpClient.DeleteAsync(requestUri);
			return await createResponse(response);
		}
		
		public async Task<Response> Delete(string uri, object obj)
		{
			var requestUri = getRequestUri(uri);
			var content = new JsonContent(obj, jsonSerializerSettings);
			var request = new HttpRequestMessage { Content = content, Method = HttpMethod.Delete, RequestUri = requestUri };
			var response = await HttpClient.SendAsync(request);
			return await createResponse(response);
		}

		public async Task<Response<T>> Delete<T>(string uri)
		{
			var requestUri = getRequestUri(uri);
			var response = await HttpClient.DeleteAsync(requestUri);
			return await createResponse<T>(response);
		}

		public async Task<Response<TSuccessModel, TBadRequestErrorModel>> Delete<TSuccessModel, TBadRequestErrorModel>(string uri)
		{
			var requestUri = getRequestUri(uri);
			var response = await HttpClient.DeleteAsync(requestUri);
			return await createResponse<TSuccessModel, TBadRequestErrorModel>(response);
		}

		public async Task<Response<T>> Delete<T>(string uri, object obj)
		{
			var requestUri = getRequestUri(uri);
			var content = new JsonContent(obj, jsonSerializerSettings);
			var request = new HttpRequestMessage { Content = content, Method = HttpMethod.Delete, RequestUri = requestUri };
			var response = await HttpClient.SendAsync(request);
			return await createResponse<T>(response);
		}

		public async Task<Response<TSuccessModel, TBadRequestErrorModel>> Delete<TSuccessModel, TBadRequestErrorModel>(string uri, object obj)
		{
			var requestUri = getRequestUri(uri);
			var content = new JsonContent(obj, jsonSerializerSettings);
			var request = new HttpRequestMessage { Content = content, Method = HttpMethod.Delete, RequestUri = requestUri };
			var response = await HttpClient.SendAsync(request);
			return await createResponse<TSuccessModel, TBadRequestErrorModel>(response);
		}
		
		public HttpResponseMessage SimpleDeleteSync(string uri)
		{
			var requestUri = getRequestUri(uri);
			var response = HttpClient.DeleteAsync(requestUri).GetSynchronousResult();
			return response;
		}

		public HttpResponseMessage SimpleDeleteSync(string uri, object obj)
		{
			var requestUri = getRequestUri(uri);
			var content = new JsonContent(obj, jsonSerializerSettings);
			var request = new HttpRequestMessage { Content = content, Method = HttpMethod.Delete, RequestUri = requestUri };
			var response = HttpClient.SendAsync(request).GetSynchronousResult();
			return response;
		}

		public async Task<HttpResponseMessage> SimpleDelete(string uri)
		{
			var requestUri = getRequestUri(uri);
			var response = await HttpClient.DeleteAsync(requestUri);
			return response;
		}
		
		public async Task<HttpResponseMessage> SimpleDelete(string uri, object obj)
		{
			var requestUri = getRequestUri(uri);
			var content = new JsonContent(obj, jsonSerializerSettings);
			var request = new HttpRequestMessage { Content = content, Method = HttpMethod.Delete, RequestUri = requestUri };
			var response = await HttpClient.SendAsync(request);
			return response;
		}

		#endregion

        private async Task<Response> createResponse(HttpResponseMessage response)
        {
	        if (response.IsSuccessStatusCode)
	        {
		        return new Response
		        {
			        IsSuccessStatusCode = response.IsSuccessStatusCode,
			        StatusCode = response.StatusCode
		        };
	        }
	        return new Response
	        {
		        IsSuccessStatusCode = response.IsSuccessStatusCode,
		        StatusCode = response.StatusCode,
		        ErrorMessage = await response.Content.ReadAsStringAsync()
	        };
        }

        private Response createResponseSync(HttpResponseMessage response)
        {
	        if (response.IsSuccessStatusCode)
	        {
		        return new Response
		        {
			        IsSuccessStatusCode = response.IsSuccessStatusCode,
			        StatusCode = response.StatusCode
		        };
	        }
	        return new Response
	        {
		        IsSuccessStatusCode = response.IsSuccessStatusCode,
		        StatusCode = response.StatusCode,
		        ErrorMessage = response.Content.ReadAsStringAsync().GetSynchronousResult()
	        };
        }

        private async Task<Response<T>> createResponse<T>(HttpResponseMessage response)
        {
	        if (response.IsSuccessStatusCode)
	        {
		        return new Response<T>
		        {
			        IsSuccessStatusCode = response.IsSuccessStatusCode,
			        StatusCode = response.StatusCode,
			        Content = JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync())
		        };
	        }
	        return new Response<T>
	        {
		        IsSuccessStatusCode = response.IsSuccessStatusCode,
		        StatusCode = response.StatusCode,
		        ErrorMessage = await response.Content.ReadAsStringAsync()
	        };
        }

        private Response<T> createResponseSync<T>(HttpResponseMessage response)
        {
	        if (response.IsSuccessStatusCode)
	        {
		        return new Response<T>
		        {
			        IsSuccessStatusCode = response.IsSuccessStatusCode,
			        StatusCode = response.StatusCode,
			        Content = JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().GetSynchronousResult())
		        };
	        }
	        return new Response<T>
	        {
		        IsSuccessStatusCode = response.IsSuccessStatusCode,
		        StatusCode = response.StatusCode,
		        ErrorMessage = response.Content.ReadAsStringAsync().GetSynchronousResult()
	        };
        }

        private async Task<Response<TSuccessModel, TBadRequestErrorModel>> createResponse<TSuccessModel, TBadRequestErrorModel>(HttpResponseMessage response)
        {
	        if (response.IsSuccessStatusCode)
	        {
		        return new Response<TSuccessModel, TBadRequestErrorModel>
		        {
			        IsSuccessStatusCode = response.IsSuccessStatusCode,
			        StatusCode = response.StatusCode,
			        Content = JsonConvert.DeserializeObject<TSuccessModel>(await response.Content.ReadAsStringAsync())
		        };
	        }

	        if (response.StatusCode == HttpStatusCode.BadRequest)
	        {
		        return new Response<TSuccessModel, TBadRequestErrorModel>
		        {
			        IsSuccessStatusCode = response.IsSuccessStatusCode,
			        StatusCode = response.StatusCode,
			        ErrorModel = JsonConvert.DeserializeObject<TBadRequestErrorModel>(await response.Content.ReadAsStringAsync())
		        };
	        }

	        return new Response<TSuccessModel, TBadRequestErrorModel>
	        {
		        IsSuccessStatusCode = response.IsSuccessStatusCode,
		        StatusCode = response.StatusCode,
		        ErrorMessage = await response.Content.ReadAsStringAsync()
	        };
        }

        private Response<TSuccessModel, TBadRequestErrorModel> createResponseSync<TSuccessModel, TBadRequestErrorModel>(HttpResponseMessage response)
        {
	        if (response.IsSuccessStatusCode)
	        {
		        return new Response<TSuccessModel, TBadRequestErrorModel>
		        {
			        IsSuccessStatusCode = response.IsSuccessStatusCode,
			        StatusCode = response.StatusCode,
			        Content = JsonConvert.DeserializeObject<TSuccessModel>(response.Content.ReadAsStringAsync().GetSynchronousResult())
		        };
	        }

	        if (response.StatusCode == HttpStatusCode.BadRequest)
	        {
		        return new Response<TSuccessModel, TBadRequestErrorModel>
		        {
			        IsSuccessStatusCode = response.IsSuccessStatusCode,
			        StatusCode = response.StatusCode,
			        ErrorModel = JsonConvert.DeserializeObject<TBadRequestErrorModel>(response.Content.ReadAsStringAsync().GetSynchronousResult())
		        };
	        }

	        return new Response<TSuccessModel, TBadRequestErrorModel>
	        {
		        IsSuccessStatusCode = response.IsSuccessStatusCode,
		        StatusCode = response.StatusCode,
		        ErrorMessage = response.Content.ReadAsStringAsync().GetSynchronousResult()
	        };
        }

        private Uri getRequestUri(string uri)
        {
            return baseUri == null ? new Uri(uri) : new Uri(baseUri, uri);
        }
        
        private string getQueryString(string uri, object queryStringValues)
        {
	        return queryStringValues == null ? string.Empty : queryStringValues.ToQueryString(uri.IndexOf('?')>=0);
        }
    }
}