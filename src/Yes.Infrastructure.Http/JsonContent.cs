﻿using System.Net.Http;
using System.Text;
using Newtonsoft.Json;

namespace Yes.Infrastructure.Http
{
    public class JsonContent : StringContent
    {
        public JsonContent(object obj, JsonSerializerSettings jsonSerializerSettings)
            : base(JsonConvert.SerializeObject(obj, jsonSerializerSettings), Encoding.UTF8, "application/json")
        {
        }
    }
}