﻿using System.Net;
using Newtonsoft.Json;

namespace Yes.Infrastructure.Http
{
	public class Response
	{
		public HttpStatusCode StatusCode { get; set; }
		public bool IsSuccessStatusCode { get; set; }
		public string ErrorMessage { get; set; }

		public override string ToString()
		{
			return JsonConvert.SerializeObject(this);
		}

		public static Response Ok()
		{
			return new Response
			{
				StatusCode = HttpStatusCode.OK,
				IsSuccessStatusCode = true
			};
		}
		
		public static Response BadRequest()
		{
			return new Response
			{
				StatusCode = HttpStatusCode.BadRequest,
				IsSuccessStatusCode = false
			};
		}
		
		public static Response BadRequest(string errorMessage)
		{
			return new Response
			{
				StatusCode = HttpStatusCode.BadRequest,
				ErrorMessage = errorMessage,
				IsSuccessStatusCode = false
			};
		}
		
		public static Response Forbidden()
		{
			return new Response
			{
				StatusCode = HttpStatusCode.Forbidden,
				IsSuccessStatusCode = false
			};
		}
		
		public static Response NotFound()
		{
			return new Response
			{
				StatusCode = HttpStatusCode.NotFound,
				IsSuccessStatusCode = false
			};
		}
		
		public static Response InternalServerError()
		{
			return new Response
			{
				StatusCode = HttpStatusCode.InternalServerError,
				IsSuccessStatusCode = false
			};
		}
		
		public static Response InternalServerError(string errorMessage)
		{
			return new Response
			{
				StatusCode = HttpStatusCode.InternalServerError,
				ErrorMessage = errorMessage,
				IsSuccessStatusCode = false
			};
		}
	}

	public class Response<T> : Response
	{
		public T Content { get; set; }
		
		public static Response<T> Ok(T content)
		{
			return new Response<T>
			{
				StatusCode = HttpStatusCode.OK,
				IsSuccessStatusCode = true,
				Content = content
			};
		}
		
		public new static Response<T> BadRequest()
		{
			return new Response<T>
			{
				StatusCode = HttpStatusCode.BadRequest,
				IsSuccessStatusCode = false
			};
		}
		
		public new static Response<T> BadRequest(string errorMessage)
		{
			return new Response<T>
			{
				StatusCode = HttpStatusCode.BadRequest,
				ErrorMessage = errorMessage,
				IsSuccessStatusCode = false
			};
		}
		
		public static Response<T> Unauthorized()
		{
			return new Response<T>
			{
				StatusCode = HttpStatusCode.Unauthorized,
				IsSuccessStatusCode = false
			};
		}

		public static Response<T> Unauthorized(string errorMessage)
		{
			return new Response<T>
			{
				StatusCode = HttpStatusCode.Unauthorized,
				ErrorMessage = errorMessage,
				IsSuccessStatusCode = false
			};
		}

		public new static Response<T> Forbidden()
		{
			return new Response<T>
			{
				StatusCode = HttpStatusCode.Forbidden,
				IsSuccessStatusCode = false
			};
		}

		public new static Response<T> NotFound()
		{
			return new Response<T>
			{
				StatusCode = HttpStatusCode.NotFound,
				IsSuccessStatusCode = false
			};
		}
		
		public new static Response<T> InternalServerError()
		{
			return new Response<T>
			{
				StatusCode = HttpStatusCode.InternalServerError,
				IsSuccessStatusCode = false
			};
		}
		
		public new static Response<T> InternalServerError(string errorMessage)
		{
			return new Response<T>
			{
				StatusCode = HttpStatusCode.InternalServerError,
				ErrorMessage = errorMessage,
				IsSuccessStatusCode = false
			};
		}
	}

	public class Response<TSuccessModel, TBadRequestErrorModel> : Response<TSuccessModel>
	{
		public TBadRequestErrorModel ErrorModel { get; set; }
		
		public new static Response<TSuccessModel, TBadRequestErrorModel> Ok(TSuccessModel content)
		{
			return new Response<TSuccessModel, TBadRequestErrorModel>
			{
				StatusCode = HttpStatusCode.OK,
				IsSuccessStatusCode = true,
				Content = content
			};
		}
		
		public new static Response<TSuccessModel, TBadRequestErrorModel> BadRequest(string errorMessage)
		{
			return new Response<TSuccessModel, TBadRequestErrorModel>
			{
				StatusCode = HttpStatusCode.BadRequest,
				ErrorMessage = errorMessage,
				IsSuccessStatusCode = false
			};
		}
		
		public static Response<TSuccessModel, TBadRequestErrorModel> BadRequest(TBadRequestErrorModel errorModel)
		{
			return new Response<TSuccessModel, TBadRequestErrorModel>
			{
				StatusCode = HttpStatusCode.BadRequest,
				IsSuccessStatusCode = false,
				ErrorModel = errorModel
			};
		}
		
		public static Response<TSuccessModel, TBadRequestErrorModel> BadRequest(TBadRequestErrorModel errorModel, string errorMessage)
		{
			return new Response<TSuccessModel, TBadRequestErrorModel>
			{
				StatusCode = HttpStatusCode.BadRequest,
				ErrorMessage = errorMessage,
				IsSuccessStatusCode = false,
				ErrorModel = errorModel
			};
		}

		public new static Response<TSuccessModel, TBadRequestErrorModel> NotFound()
		{
			return new Response<TSuccessModel, TBadRequestErrorModel>
			{
				StatusCode = HttpStatusCode.NotFound,
				IsSuccessStatusCode = false
			};
		}

		public new static Response<TSuccessModel, TBadRequestErrorModel> InternalServerError()
		{
			return new Response<TSuccessModel, TBadRequestErrorModel>
			{
				StatusCode = HttpStatusCode.InternalServerError,
				IsSuccessStatusCode = false
			};
		}
		
		public new static Response<TSuccessModel, TBadRequestErrorModel> InternalServerError(string errorMessage)
		{
			return new Response<TSuccessModel, TBadRequestErrorModel>
			{
				StatusCode = HttpStatusCode.InternalServerError,
				ErrorMessage = errorMessage,
				IsSuccessStatusCode = false
			};
		}
		
	}
}
