﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Yes.Infrastructure.Http.Extensions
{
    public static class ObjectExtensions
    {
        /// <summary>
        /// Преобразоывает объект в строку запроса
        /// </summary>
        /// <param name="isAppend">Сформировать строку запроса как новую(начинается с ?), или для конкатенации с суже существующей (начинается с &)</param>
        /// <returns></returns>
        public static string ToQueryString(this object paramsObject, bool isAppend = false)
        {
            var result = new List<string>();
            var props = paramsObject.GetType().GetProperties().Where(p => p.GetValue(paramsObject, null) != null);

            foreach (var p in props)
            {
                var value = p.GetValue(paramsObject, null);

                string stringValue;
                switch (value)
                {
                    case DateTime dt:
                        stringValue = dt.ToString(@"yyyy'-'MM'-'dd'T'HH':'mm':'ss.FFFFFFFK");
                        result.Add($"{p.Name}={WebUtility.UrlEncode(stringValue)}");
                        break;
                    case IList list://todo add recursion
                        foreach (var item in list)
                        {
                            stringValue = item.ToString();
                            result.Add($"{p.Name}={WebUtility.UrlEncode(stringValue)}");
                        }
                        break;
                    default:
                        stringValue = value.ToString();
                        result.Add($"{p.Name}={WebUtility.UrlEncode(stringValue)}");
                        break;
                }
            }

            if (result.Count == 0)
                return string.Empty;

            return (isAppend ? "&" : "?") + string.Join("&", result.ToArray());
        }
    }
}
