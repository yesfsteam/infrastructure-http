﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Yes.Infrastructure.Http.Extensions
{
    public static class ServiceCollectionExtensions
    {
        private const string DEFAULT_TIMEOUT = "00:01:40";
        
        public static IHttpClientBuilder AddHttpClientFromConfiguration<TClient, TImplementation>(this IServiceCollection services, IConfiguration configuration, string configurationSectionName = null, string clientName = null)
            where TClient : class
            where TImplementation : class, TClient
        {
            var clientConfigurationSection = configuration.GetSection(configurationSectionName ?? typeof(TImplementation).Name);
            return services.AddHttpClient<TClient, TImplementation>(clientName ?? typeof(TImplementation).Name, c =>
            {
                c.BaseAddress = new Uri(clientConfigurationSection["Url"]);
                c.Timeout = TimeSpan.Parse(clientConfigurationSection.GetValue<string>("Timeout") ?? DEFAULT_TIMEOUT);
            });
        }
    }
}
