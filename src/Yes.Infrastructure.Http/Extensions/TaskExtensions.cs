﻿﻿using System.Threading.Tasks;

 namespace Yes.Infrastructure.Http.Extensions
{
    public static class TaskExtensions
    {
        public static T GetSynchronousResult<T>(this Task<T> task)
        {
            return Task.Run(async () => await task).ConfigureAwait(false).GetAwaiter().GetResult();
        }
    }
}
